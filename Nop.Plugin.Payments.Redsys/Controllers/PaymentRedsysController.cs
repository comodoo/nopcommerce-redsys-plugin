﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.Redsys.Models;
using Nop.Services.Stores;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.Payments;
using Nop.Web.Framework.Controllers;
using Nop.Core.Domain.Messages;
using Nop.Services.Localization;
using System.Linq.Expressions;
using Nop.Core.Configuration;
using Nop.Services.Messages;

namespace Nop.Plugin.Payments.Redsys.Controllers
{
    public class PaymentRedsysController : BasePaymentController
    {
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;        
        private readonly IPaymentService _paymentService;
        private readonly ILogger _logger;
        private readonly ILocalizationService _localizationService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly PaymentSettings _paymentSettings;

        public PaymentRedsysController(
            IWorkContext workContext,
            IStoreService storeService,
            IPaymentService paymentService,
            ISettingService settingService,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            PaymentSettings paymentSettings,
            RedsysPaymentSettings redsysPaymentSettings,
            ILogger logger)
        {
            this._workContext = workContext;
            this._storeService = storeService;

            this._paymentService = paymentService;
            this._settingService = settingService;            
            this._localizationService = localizationService;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._paymentSettings = paymentSettings;
            this._logger = logger;
        }


        #region Admin

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var redsysPaymentSettings = _settingService.LoadSetting<RedsysPaymentSettings>(storeScope);
            
            var model = new ConfigurationModel()
            {                
                MerchantCode = redsysPaymentSettings.MerchantCode,
                MerchantName = redsysPaymentSettings.MerchantName,
                OrderPrefix = redsysPaymentSettings.OrderPrefix,
                PaymentReferenceText = redsysPaymentSettings.PaymentReferenceText,
                Terminal = redsysPaymentSettings.Terminal,
                AdditionalFee = redsysPaymentSettings.AdditionalFee,
                AdditionalFeePercentage = redsysPaymentSettings.AdditionalFeePercentage,
                IsTestMode = redsysPaymentSettings.IsTestMode,
                TestEncryptionKey = redsysPaymentSettings.TestEncryptionKey,
                EncryptionKey = redsysPaymentSettings.EncryptionKey,
            };

            string protocol = "http";
            var currentStore = _storeService.GetStoreById(storeScope) ?? _storeService.GetAllStores().First();
            if (currentStore.SslEnabled)
                protocol = "https";

            model.AuthorizePaymentUrl = Url.RouteUrl("Plugin.Payments.Redsys.Return", null, protocol);

            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.MerchantCode_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.MerchantCode, storeScope);
                model.MerchantName_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.MerchantName, storeScope);
                model.OrderPrefix_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.OrderPrefix, storeScope);
                model.Terminal_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.Terminal, storeScope);
                model.PaymentReferenceText_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.PaymentReferenceText, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.IsTestMode_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.IsTestMode, storeScope);
                model.TestEncryptionKey_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.TestEncryptionKey, storeScope);
                model.EncryptionKey_OverrideForStore = _settingService.SettingExists(redsysPaymentSettings, x => x.EncryptionKey, storeScope);
            }

            return View(model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {

            if (!ModelState.IsValid)
                return Configure();

            String paymentUrl = String.Empty;
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var redsysPaymentSettings = _settingService.LoadSetting<RedsysPaymentSettings>(storeScope);
            var currentStore = _storeService.GetStoreById(storeScope) ?? _storeService.GetAllStores().First();

            string protocol = "http";
            if (currentStore.SslEnabled)
                protocol = "https";
            
            model.AuthorizePaymentUrl = Url.RouteUrl("Plugin.Payments.Redsys.Return", null, protocol);

            redsysPaymentSettings.MerchantCode = model.MerchantCode;
            redsysPaymentSettings.MerchantName = model.MerchantName;
            redsysPaymentSettings.OrderPrefix = model.OrderPrefix;
            redsysPaymentSettings.Terminal = model.Terminal;
            redsysPaymentSettings.PaymentReferenceText = model.PaymentReferenceText;
            redsysPaymentSettings.AdditionalFee = model.AdditionalFee;

            redsysPaymentSettings.IsTestMode = model.IsTestMode;
            redsysPaymentSettings.TestEncryptionKey = model.TestEncryptionKey;
            redsysPaymentSettings.EncryptionKey = model.EncryptionKey;

            SaveSetting(redsysPaymentSettings, x => x.MerchantCode, storeScope, model.MerchantCode_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.MerchantName, storeScope, model.MerchantName_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.OrderPrefix, storeScope, model.OrderPrefix_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.Terminal, storeScope, model.Terminal_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.PaymentReferenceText, storeScope, model.PaymentReferenceText_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.AdditionalFee, storeScope, model.AdditionalFee_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.AdditionalFeePercentage, storeScope, model.AdditionalFeePercentage_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.IsTestMode, storeScope, model.IsTestMode_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.TestEncryptionKey, storeScope, model.TestEncryptionKey_OverrideForStore);
            SaveSetting(redsysPaymentSettings, x => x.EncryptionKey, storeScope, model.EncryptionKey_OverrideForStore);

            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion

     
        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            return View();
        }

        #region BasePaymentController implementation

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            return paymentInfo;
        }

        #endregion

        /// <summary>
        /// When a payment is done, the gateway post to this, here we can mark the payment as done
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [ValidateInput(false)]        
        public ActionResult Return(FormCollection form)
        {
            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.Redsys") as RedsysPaymentProcessor;
            if (processor == null ||
                !processor.IsPaymentMethodActive(_paymentSettings) || !processor.PluginDescriptor.Installed)
                throw new NopException("Redsys payment plugin module cannot be loaded");

            if (processor.TryValidatePayment(this.Request.Params))
                return Content(String.Empty);
            else
                throw new NopException("Error cannot confirm payment");
        }

        /// <summary>
        /// When a payment return error, the gateway post to this
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult RedsysError()
        {
            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.Redsys") as RedsysPaymentProcessor;
            if (processor == null ||
                !processor.IsPaymentMethodActive(_paymentSettings) || !processor.PluginDescriptor.Installed)
                throw new NopException("Redsys module cannot be loaded");

            _logger.Warning("Proceso de pago con errores en la pasarela");

            ErrorModel model = new ErrorModel();
            EmailAccount email = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            model.ContactEmail = email.Email;

            return View(model);
        }


        private void SaveSetting<T, TPropType>(T settings, Expression<Func<T, TPropType>> keySelector, int storeScope, bool overrideForStore) where T : ISettings, new()
        {
            if (overrideForStore || storeScope == 0)
                _settingService.SaveSetting<T, TPropType>(settings, keySelector, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(settings, keySelector, storeScope);
        }

    }
}