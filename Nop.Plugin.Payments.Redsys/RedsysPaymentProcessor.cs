﻿using System;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Redsys.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Payments;
using Nop.Web.Framework;
using System.Web.Mvc;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using Nop.Services.Localization;
using Nop.Services.Logging;
using System.Collections.Specialized;
using Nop.Services.Orders;

namespace Nop.Plugin.Payments.Redsys
{
    /// <summary>
    /// Redsys payment processor
    /// </summary>
    public class RedsysPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private static object o = new object();
        private readonly RedsysPaymentSettings _redsysPaymentSettings;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;
        private readonly HttpContextBase _httpContext;
        private UrlHelper _urlHelper;
        private readonly IStoreContext _storeContext;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Properties

        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }

        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }

        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.NotSupported;
            }
        }

        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Redirection;
            }
        }

        public bool SupportOrdersWithoutShipping
        {
            get
            {
                return true;
            }
        }

        #endregion

        #region Ctor

        public RedsysPaymentProcessor(
            IWorkContext workContext,
            IWebHelper webHelper,
            RedsysPaymentSettings redsysPaymentSettings,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            ILogger logger,
            ISettingService settingService,
            ILocalizationService localizationService,
            HttpContextBase httpContext,
            IStoreContext storeContext)
        {
            this._settingService = settingService;
            this._workContext = workContext;
            this._webHelper = webHelper;
            this._redsysPaymentSettings = redsysPaymentSettings;
            this._logger = logger;
            this._httpContext = httpContext;
            this._storeContext = storeContext;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._localizationService = localizationService;
        }

        #endregion

        #region IPaymentMethod implementation

        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            return false;
        }

        public bool SkipPaymentInfo
        {
            get
            {
                return false;
            }
        }

        public string PaymentMethodDescription
        {
            get
            {
                return _localizationService.GetResource("Plugin.Payments.Redsys.PaymentMethodDescription");
            }
        }

        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.NewPaymentStatus = PaymentStatus.Pending;
            return result;
        }

        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart, _redsysPaymentSettings.AdditionalFee, _redsysPaymentSettings.AdditionalFeePercentage);
            return result;
        }

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            return false;
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentRedsys";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Redsys.Controllers" }, { "area", null } };
        }

        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentRedsys";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Redsys.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentRedsysController);
        }

        public override void Install()
        {
            var settings = new RedsysPaymentSettings()
            {
                TestUrl = RedsysPaymentSettings.DefaultTestUrl,
                Url = RedsysPaymentSettings.DefaultUrl,
                TestEncryptionKey = RedsysPaymentSettings.DefaultTestEncryptionKey,
                IsTestMode = true
            };
            _settingService.SaveSetting(settings);


            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.PaymentMethodDescription", "Pago con tarjeta");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.RedirectionTip", "Al confirmar su pedido será redireccionado a la página de nuestra pasarela de pago");

            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.RedirectionTip", "Al confirmar su pedido será redireccionado a la página de nuestra pasarela de pago");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.PostProcessErrorMessage", "Su pago no ha podido ser procesado");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.ConfigurationTip", "Si es la primera vez que utiliza la pasarela de pago es posible que su banco le obligue a completar primero una serie de pedidos en el modo de pruebas. Revise la documentación que tiene de su banco");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.OnlineConfigurationTip", "Para que los pagos sean autorizados automáticamente debe activar la \"Notificación ON-LINE\" en su consola de administración del TPV de Redsys y deberá configura al menos la opción \"Con notificación ON-LINE: HTTP\", si no los pagos deberán ser confirmados en la tienda de forma manual");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.Error", "Error");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.ErrorMessage", "Se ha producido un error y su pago no ha sido procesado, por favor para continuar con su pedido póngase en contacto con nosotros a través de ");

            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.MerchantName", "Nombre del comercio");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.MerchantCode", "Número de comercio (FUC)");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.OrderPrefix", "Prefijo Nº del pedido");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.Terminal", "Terminal");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.EncryptionKey", "Clave de encriptación");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.TestEncryptionKey", "Clave de encriptación (entorno de pruebas)");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.PaymentReferenceText", "Referencia del pago");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.IsTestMode", "Modo de pruebas");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFee", "Coste adicional");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFeePercentage", "Usar coste adicional como porcentaje del valor del pedido");

            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.MerchantName.hint", "Nombre del comercio que se mostrará en la pantalla al realizar el pago");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.MerchantCode.hint", "Identificador del comercio (FUC o MerchantCode). Facilitado por la entidad en el proceso de alta");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.OrderPrefix.hint", "Prefijo a utilizar con el número de pedido al enviar a la plataforma de pago, para permitir identificar los pedidos entre varias tiendas");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.Terminal.hint", "Número de Terminal. Facilitado por la entidad en el proceso de alta");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.EncryptionKey.hint", "Clave de encriptación (entorno real)");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.TestEncryptionKey.hint", "Clave de encriptación (entorno de pruebas)");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.PaymentReferenceText.hint", "Texto descriptivo de hasta 150 caracteres a modo de concepto/referencia del pago. Se puede usar la etiqueta %order% y será sustituida automáticamente por el número de pedido");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.IsTestMode.hint", "Indica si está activo el modo de pruebas y por tanto ninguna de las transacciones serán reales");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFee.hint", "Coste adicional aplicable a los pedidos cuando se selecciona este tipo de pago");
            this.AddOrUpdatePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFeePercentage.hint", "Calcular el coste adicional como porcentaje del total del pedido");

            base.Install();
        }

        public override void Uninstall()
        {

            _settingService.DeleteSetting<RedsysPaymentSettings>();

            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.RedirectionTip");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.PostProcessErrorMessage");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.OnlineConfigurationTip");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.ConfigurationTip");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.Error");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.ErrorMessage");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.MerchantName");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.MerchantCode");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.OrderPrefix");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.Terminal");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.EncryptionKey");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.TestEncryptionKey");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.PaymentReferenceText");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.IsTestMode");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFee");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.MerchantName.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.MerchantCode.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.Terminal.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.EncryptionKey.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.TestEncryptionKey.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.PaymentReferenceText.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.IsTestMode.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFee.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.AdditionalFeePercentage.hint");
            this.DeletePluginLocaleResource("Plugin.Payments.Redsys.OrderPrefix.hint");

            base.Uninstall();
        }

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            string protocol = "http";
            if (_storeContext.CurrentStore.SslEnabled)
                protocol = "https";

            string returnUrl = RouteUrl("Plugin.Payments.Redsys.Return", null, protocol);
            string okUrl = RouteUrl("CheckoutCompleted", new { orderId = postProcessPaymentRequest.Order.Id }, protocol);
            string koUrl = RouteUrl("Plugin.Payments.Redsys.Error", null, protocol);

            string order = string.Format("{0}{1:0000}", _redsysPaymentSettings.OrderPrefix, postProcessPaymentRequest.Order.Id);
            string merchantName = _redsysPaymentSettings.MerchantName;
            string amount = ((int)Convert.ToInt64(postProcessPaymentRequest.Order.OrderTotal * 100)).ToString();
            string merchantCode = _redsysPaymentSettings.MerchantCode;
            string terminal = _redsysPaymentSettings.Terminal;
            string currency = RedsysHelper.Iso4217ToIso3166_1CurrencyCode(postProcessPaymentRequest.Order.CustomerCurrencyCode);
            string transactionType = "0"; //Autorización;

            string encryptionKey = _redsysPaymentSettings.CurrentEncriptionKey;
            string signatureVersion = _redsysPaymentSettings.SignatureVersion;

            var redsysApi = new RedsysAPI();

            redsysApi.SetParameter("DS_MERCHANT_AMOUNT", amount);
            redsysApi.SetParameter("DS_MERCHANT_ORDER", order);
            redsysApi.SetParameter("DS_MERCHANT_MERCHANTCODE", merchantCode);
            redsysApi.SetParameter("DS_MERCHANT_CURRENCY", currency);
            redsysApi.SetParameter("DS_MERCHANT_TRANSACTIONTYPE", transactionType);
            redsysApi.SetParameter("DS_MERCHANT_TERMINAL", terminal);
            redsysApi.SetParameter("DS_MERCHANT_MERCHANTURL", returnUrl);
            redsysApi.SetParameter("DS_MERCHANT_URLOK", okUrl);
            redsysApi.SetParameter("DS_MERCHANT_URLKO", koUrl);

            redsysApi.SetParameter("DS_MERCHANT_PRODUCTDESCRIPTION", _redsysPaymentSettings.PaymentReferenceText.Replace("%order%", order).Length > 125 ? _redsysPaymentSettings.PaymentReferenceText.Replace("%order%", order).Substring(0, 125) : _redsysPaymentSettings.PaymentReferenceText.Replace("%order%", order));
            redsysApi.SetParameter("DS_MERCHANT_TITULAR", merchantName);
            redsysApi.SetParameter("DS_MERCHANT_MERCHANTNAME", merchantName);

            string consumerLanguage = RedsysHelper.GetLanguageCodeOrDefault(_workContext.WorkingLanguage != null ? _workContext.WorkingLanguage.LanguageCulture : null);
            redsysApi.SetParameter("DS_MERCHANT_CONSUMERLANGUAGE", consumerLanguage);

            string merchantParameters = redsysApi.createMerchantParameters();
            string signature = redsysApi.createMerchantSignature(_redsysPaymentSettings.CurrentEncriptionKey);

            //Creamos el POST
            var remotePostHelper = new RemotePost();
            remotePostHelper.FormName = "form1";
            remotePostHelper.Url = _redsysPaymentSettings.CurrentUrl;
            remotePostHelper.Add("Ds_MerchantParameters", merchantParameters);
            remotePostHelper.Add("Ds_SignatureVersion", signatureVersion);
            remotePostHelper.Add("Ds_Signature", signature);

            remotePostHelper.Post();

            string note = "Redsys payment request : " + redsysApi.decodeMerchantParameters(merchantParameters);
            try
            {
                var currentOrder = _orderService.GetOrderById(postProcessPaymentRequest.Order.Id);
                currentOrder.OrderNotes.Add(new OrderNote()
                {
                    Note = note,
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(currentOrder);
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("Cannot add note to order during Redsys request due to {0}. Note: {1}", ex.Message, note), ex);
            }
        }

        #endregion

        #region Methods

        public bool TryValidatePayment(NameValueCollection values)
        {
            if (values == null || values.Count == 0)
                return false;

            bool ok = false;
            string errorMessage = String.Empty;

            string signature = values["Ds_Signature"];
            string version = values["Ds_SignatureVersion"];
            string merchantParameters = values["Ds_MerchantParameters"];

            var api = new RedsysAPI();
            string verificationSignature = !String.IsNullOrEmpty(merchantParameters) ? api.createMerchantSignatureNotif(_redsysPaymentSettings.CurrentEncriptionKey, merchantParameters) : String.Empty;
            string orderCode = api.GetParameter("Ds_Order") ?? "";
            string response = api.GetParameter("Ds_Response") ?? "";

            if (String.IsNullOrEmpty(version) || !version.Equals(_redsysPaymentSettings.SignatureVersion, StringComparison.InvariantCultureIgnoreCase))
            {
                string datosRecibidos = String.Format("Parameters: {0}\n Signature: {1},\n Version: {2} ", api.decodeMerchantParameters(merchantParameters), signature, version);
                errorMessage = string.Format("Redsys payment authorization error for order \"{0}\". The signature version does not match. Signature version received: {1}  Signature version wanted: {2}. Data received: {3}", orderCode, version, _redsysPaymentSettings.SignatureVersion, datosRecibidos);
            }
            else if (String.IsNullOrWhiteSpace(verificationSignature) || !verificationSignature.Equals(signature))
            {
                string datosRecibidos = String.Format("Parameters: {0}\n Signature: {1},\n Version: {2} ", api.decodeMerchantParameters(merchantParameters), signature, version);
                _logger.Error(string.Format("Redsys payment authorization error for order \"{0}\". The signature sent and received do not match. Received: {1}  Wanted: {2}. Data received: {3}", orderCode, signature, verificationSignature, datosRecibidos));
            }
            else
            {
                if (!String.IsNullOrEmpty(_redsysPaymentSettings.OrderPrefix))
                    orderCode = orderCode.Replace(_redsysPaymentSettings.OrderPrefix, "");

                int orderId = -1;
                int.TryParse(orderCode, out orderId);
                Order order = _orderService.GetOrderById(orderId);

                int responseId = -1;
                int.TryParse(response, out responseId);
                if (responseId < 0 || responseId > 99)
                {
                    _logger.Error(string.Format("Payment not authorized. Error code sent by Redsys: {0}. \n {1}", orderCode, response));

                    if (order != null)
                    {
                        order.OrderNotes.Add(new OrderNote()
                        {
                            Note = String.Format("Payment not authorized. Error code sent by Redsys: {0}. \n {1}", response, api.decodeMerchantParameters(merchantParameters)),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        _orderService.UpdateOrder(order);
                    }
                }
                else
                {
                    if (order == null)
                    {
                        string datosRecibidos = String.Format("Parameters: {0}\n Signature: {1},\n Version: {2} ", api.decodeMerchantParameters(merchantParameters), signature, version);
                        _logger.Error(string.Format("Redsys payment authorization error for order \"{0}\". The order does not exists. Data received: {1}", orderCode, datosRecibidos));
                    }
                    else
                    {
                        ok = true;

                        order.OrderNotes.Add(new OrderNote()
                        {
                            Note = "Redsys payment authorized with: " + api.decodeMerchantParameters(merchantParameters),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        _orderService.UpdateOrder(order);

                        //Lo marcamos como pagado
                        if (_orderProcessingService.CanMarkOrderAsPaid(order))
                            _orderProcessingService.MarkOrderAsPaid(order);
                    }
                }
            }

            return ok;
        }

        private string RouteUrl(string routeName, object routeValues, string protocol)
        {
            if (_urlHelper == null)
            {
                lock (o)
                {
                    try
                    {
                        if (_httpContext.Request != null)
                            _urlHelper = new UrlHelper(_httpContext.Request.RequestContext);

                    }
                    catch (Exception exc)
                    {
                        _logger.Error("Cannot instantiate new UrlHelper", exc);
                    }
                }
            }

            if (_urlHelper != null)
            {
                if (routeValues != null || !String.IsNullOrEmpty(protocol))
                    return _urlHelper.RouteUrl(routeName, routeValues, protocol);
                else
                    return _urlHelper.RouteUrl(routeName);
            }

            return String.Empty;
        }

        #endregion
    }
}
