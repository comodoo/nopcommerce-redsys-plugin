﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Redsys.Models
{
    public class ErrorModel : BaseNopModel
    {
        public string ContactEmail{get;set;}
    }
}
