﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Framework;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Payments.Redsys.Models
{
    public class ConfigurationModel : BaseNopModel
    {

        public int ActiveStoreScopeConfiguration
        {
            get;
            set;
        }

        public string AuthorizePaymentUrl
        {
            get; set;
        }
        
        [NopResourceDisplayName("Plugin.Payments.Redsys.MerchantName")]
        public string MerchantName { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.OrderPrefix")]
        [MaxLength(3)]
        public string OrderPrefix
        {
            get;
            set;
        }

        [NopResourceDisplayName("Plugin.Payments.Redsys.MerchantCode")]
        public string MerchantCode { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.Terminal")]
        public string Terminal { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.EncryptionKey")]
        public string EncryptionKey { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.TestEncryptionKey")]
        public string TestEncryptionKey { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.PaymentReferenceText")]
        public string PaymentReferenceText { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.IsTestMode")]
        public bool IsTestMode { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.AdditionalFee")]
        public decimal AdditionalFee { get; set; }

        [NopResourceDisplayName("Plugin.Payments.Redsys.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage
        {
            get; set;
        }        

        public bool MerchantCode_OverrideForStore
        {
            get;
            set;
        }
        public bool MerchantName_OverrideForStore
        {
            get;
            set;
        }
        public bool Terminal_OverrideForStore
        {
            get;
            set;
        }
        public bool PaymentReferenceText_OverrideForStore
        {
            get;
            set;
        }
        public bool AdditionalFee_OverrideForStore
        {
            get;
            set;
        }
        public bool IsTestMode_OverrideForStore
        {
            get;
            set;
        }
        public bool EncryptionKey_OverrideForStore
        {
            get;
            set;
        }
        public bool TestEncryptionKey_OverrideForStore
        {
            get;
            set;
        }
        public bool AdditionalFeePercentage_OverrideForStore
        {
            get;
            set;
        }
        public bool OrderPrefix_OverrideForStore
        {
            get;
            set;
        }       
    }
}